# CV

## Kevin Landry

### Description
Création d'un cv personnel.

### Organisation des fichier

* index.php (page d'accueil)
* style.css
    * /images
        * /logo
          * c4d.png
          * css.png
          * html5.png
          * js.png
          * php.png
          * python.png
        * batman-profil.jpg
        * pattern.png

**1 - Indentation :**

Exemples de la structure d'un fichier CSS :

    body {
        font-family: Arial, sans-serif;
        font-size: 16px;
        text-align: left;
    }

**1-2 - Générales :**

* Espace après balise, classe, id, propriété, virgule
* Attention aux espaces invisibles superflues - les éviter si possible

## Auteurs/Contributeurs :

- Kevin
